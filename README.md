# TP Dockerfile
Nombre y apellido: Gonzalo Zabalgoitia

Materia: Infraestructura en la nube

## Introducción
En este documento se detallan los pasos necesarios para:
1. Crear una imagen de un servidor web con apache2
2. Desplegar la imagen
3. Configurar un proxy reverso utilizando nginx

## Requisitos (por instancia de AWS o VM)
Se van a crear dos VMs o instancias de AWS:
1. ```servidor-web```
2. ```nginx```

### Sistema operativo
- Ubuntu 24.04 LTS

### Hardware
- RAM: 1GB
- Almacenamiento: 8GB
- CPU: 1 núcleo

### Red
1. ```servidor-web```

IP pública: 54.84.24.229

IP privada: 172.31.30.63

2. ```nginx```
	
IP pública: 34.228.38.3

IP privada: 172.31.26.217

## Desarrollo

### Diagrama conceptual
Debajo se muestra un diagrama conceptual de las VMs o instancias, contenedores y su interacción entre si.

```
        +--------------+
        | +----------+ |
        | |contenedor| |
	| | apache2  | |
	| +----------+ |
        |      VM      |
	|   servidor   |
	|      web     |
	+--------------+
	        ^
		|
		|
		|
		v
	+--------------+
        |      VM      |
	|     nginx    |
	+--------------+
	        ^
		|
		|
		|
		v
	ooooooooooooooo
	o   INTERNET  o
	ooooooooooooooo

```

### Configuración de VMs
Si se está usando AWS las dos instancias tienen que estár en el mismo VPC y el mismo grupo de seguridad:
- VPC: `vpc-03dce9e7c270c0bb9`
- Grupo de seguridad: `launch-wizard-1`

Ejecutar en ambas VMs:
Actualizar el S.O. con el siguiente comando:
```sudo apt update -y && sudo apt upgrade -y```

#### Firewalls o grupos de seguridad
habilitar firewall o grupo de seguridad para que permita el acceso SSH a ambas VMs
y HTTP solo a la VM `nginx`.

### Configuración de contenedor para apache2
En la VM ```servidor-web``` hacer lo siguiente:

Instalar docker:
```sudo apt install docker.io```

Añadir usuario de la VM al grupo docker:
```sudo gpasswd -a <usuario> docker```

Reiniciar:
```sudo reboot```

#### Dockerfile
Creamos un dockerfile nuevo para nuestra imagen.
```vim dockerfile```

En el dockerfile escribimos lo siguiente:
```
FROM ubuntu

RUN apt update && apt install apache2 -y

EXPOSE 80 443

CMD ["apache2ctl", "-D", "FOREGROUND"]

```

Guardamos el archivo y salimos con `:wq`.

#### Imagen
Construimos la imagen de docker en base al dockerfile.
```docker build -t imagen-web .```

Verificamos la creación de la imagen.
```docker images```


### Despliegue de contenedor apache2
```docker run -d --name web1 -p 8000:80 imagen-web```

### Configuración del proxy reverso
Instalamos nginx

```sudo apt install nginx```

Editamos el archivo de configuración `/etc/nginx/sites-available/default` 

```vim default```

Dentro del archivo modificamos la sección `location /` para que quede de
la siguiente manera:

```
location / {
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_pass http://172.31.30.63:80;
}

```

Guardamos el archivo y salimos con `:wq`.

Reiniciamos nginx:

```sudo nginx -s reload```


## Conclusión
Configuramos un proxy reverso que dirige el tráfico a un servidor web dockerizado en 
base a una imagen que creamos.

## Fuentes
https://docs.docker.com/build/building/base-images/

https://docs.docker.com/guides/docker-concepts/building-images/writing-a-dockerfile/?highlight=dockerfile

https://docs.docker.com/engine/reference/run/

https://docs.docker.com/engine/reference/commandline/cli/

https://docs.docker.com/reference/dockerfile/

https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/

https://docs.nginx.com/nginx/admin-guide/load-balancer/http-load-balancer/
